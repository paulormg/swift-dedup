# Copyright (c) 2010-2012 OpenStack, LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" Chunk Server for Swift """

from __future__ import with_statement
import cPickle as pickle
import errno
import os
import sys
import time
import traceback
import cStringIO
import cPickle

from datetime import datetime
from eventlet import Timeout, GreenPile, sleep, tpool
from eventlet.queue import Queue
from hashlib import md5
from random import shuffle
from tempfile import mkstemp
from urllib import unquote
from contextlib import contextmanager
from xattr import getxattr, setxattr

from webob import Request, Response, UTC
from webob.exc import HTTPBadRequest, HTTPCreated, HTTPInternalServerError, \
    HTTPNotModified, HTTPNotFound, HTTPPreconditionFailed, HTTPNoContent, \
    HTTPUnprocessableEntity, HTTPMethodNotAllowed, HTTPRequestTimeout, \
    HTTPRequestEntityTooLarge, HTTPServerError, \
    HTTPOk, WSGIHTTPException, HTTPLengthRequired

from swift.common.bufferedhttp import http_connect
from swift.common.http import is_informational, is_success, is_redirection, \
    is_client_error, is_server_error, HTTP_CONTINUE, HTTP_OK, HTTP_CREATED, \
    HTTP_ACCEPTED, HTTP_PARTIAL_CONTENT, HTTP_MULTIPLE_CHOICES, \
    HTTP_BAD_REQUEST, HTTP_NOT_FOUND, HTTP_REQUESTED_RANGE_NOT_SATISFIABLE, \
    HTTP_CLIENT_CLOSED_REQUEST, HTTP_INTERNAL_SERVER_ERROR, \
    HTTP_SERVICE_UNAVAILABLE, HTTP_INSUFFICIENT_STORAGE, \
    HTTPClientDisconnect, HTTPInsufficientStorage
from swift.common.utils import mkdirs, storage_directory, fallocate, \
    split_path, ContextPool, drop_buffer_cache, lock_path, \
    renamer, normalize_timestamp, TRUE_VALUES, lock_file
from swift.common.exceptions import DiskFileError, DiskFileNotExist, \
    ChunkReadTimeout, ConnectionTimeout
from swift.common.constraints import check_chunk_store, check_mount, \
    check_fingerprint, check_utf8, HASH_ALGOS, MAX_FILE_SIZE, \
    check_chunk_put, check_float
from swift.common.controller import BaseController
from swift.common.ring import Ring
from swift.obj.replicator import quarantine_renamer

REFERENCES_FILE = 'refs'
CHUNK_IMAGE_FILE = 'image'
DATADIR = 'chunks'
PICKLE_PROTOCOL = 2
METADATA_KEY = 'user.swift.metadata'
KEEP_CACHE_SIZE = (5 * 1024 * 1024)
# keep these lower-case
DISALLOWED_HEADERS = set('content-length content-type deleted etag'.split())


def read_metadata(fd):
    """
    Helper function to read the pickled metadata from a chunk file.

    :param fd: file descriptor to load the metadata from

    :returns: dictionary of metadata
    """
    metadata = ''
    key = 0
    try:
        while True:
            metadata += getxattr(fd, '%s%s' % (METADATA_KEY, (key or '')))
            key += 1
    except IOError:
        pass
    return pickle.loads(metadata)


def write_metadata(fd, metadata):
    """
    Helper function to write pickled metadata for a chunk file.

    :param fd: file descriptor to write the metadata
    :param metadata: metadata to write
    """
    metastr = pickle.dumps(metadata, PICKLE_PROTOCOL)
    key = 0
    while metastr:
        setxattr(fd, '%s%s' % (METADATA_KEY, key or ''), metastr[:254])
        metastr = metastr[254:]
        key += 1


class ChunkFile(object):
    """
    Manage chunk files on disk.

    :param path: path to devices on the node
    :param device: device name
    :param partition: partition on the device the chunk lives in
    :param chunk_id: fingerprint of chunk
    :param keep_data_fp: if True, don't close the fp, otherwise close it
    :param disk_chunk_Size: size of chunks on file reads
    """

    def __init__(self, path, device, partition, chunk_id, logger,
                 read_meta=True, keep_data_fp=False, disk_chunk_size=65536):
        self.disk_chunk_size = disk_chunk_size
        self.name = '/' + chunk_id
        self.datadir = os.path.join(path, device,
                            storage_directory(DATADIR, partition, chunk_id))
        self.device_path = os.path.join(path, device)
        self.tmpdir = os.path.join(path, device, 'tmp')
        self.logger = logger
        self.metadata = {}
        self.meta_file = os.path.join(self.datadir, REFERENCES_FILE)
        self.data_file = None
        self.fp = None
        self.iter_etag = None
        self.started_at_0 = False
        self.read_to_eof = False
        self.quarantined_dir = None
        self.keep_cache = False
        if not os.path.exists(self.datadir):
            return

        image_path = os.path.join(self.datadir, CHUNK_IMAGE_FILE)

        if os.path.exists(image_path):
            self.data_file = image_path
        else:
            return

        if read_meta:
            self.fp = open(self.data_file, 'rb')
            self.metadata = read_metadata(self.fp)
            if not keep_data_fp:
                self.close(verify_file=False)

    def __iter__(self):
        """Returns an iterator over the data file."""
        try:
            dropped_cache = 0
            read = 0
            self.started_at_0 = False
            self.read_to_eof = False
            if self.fp.tell() == 0:
                self.started_at_0 = True
                self.iter_etag = md5()
            while True:
                chunk = self.fp.read(self.disk_chunk_size)
                if chunk:
                    if self.iter_etag:
                        self.iter_etag.update(chunk)
                    read += len(chunk)
                    if read - dropped_cache > (1024 * 1024):
                        self.drop_cache(self.fp.fileno(), dropped_cache,
                            read - dropped_cache)
                        dropped_cache = read
                    yield chunk
                else:
                    self.read_to_eof = True
                    self.drop_cache(self.fp.fileno(), dropped_cache,
                        read - dropped_cache)
                    break
        finally:
            self.close()

    def app_iter_range(self, start, stop):
        """Returns an iterator over the data file for range (start, stop)"""
        if start:
            self.fp.seek(start)
        if stop is not None:
            length = stop - start
        else:
            length = None
        for chunk in self:
            if length is not None:
                length -= len(chunk)
                if length < 0:
                    # Chop off the extra:
                    yield chunk[:length]
                    break
            yield chunk

    def _handle_close_quarantine(self):
        """Check if file needs to be quarantined"""
        try:
            self.get_data_file_size()
        except DiskFileError:
            self.quarantine()
            return
        except DiskFileNotExist:
            return

        if (self.iter_etag and self.started_at_0 and self.read_to_eof and
            'ETag' in self.metadata and
            self.iter_etag.hexdigest() != self.metadata.get('ETag')):
                self.quarantine()

    def close(self, verify_file=True):
        """
        Close the file. Will handle quarantining file if necessary.

        :param verify_file: Defaults to True. If false, will not check
                            file to see if it needs quarantining.
        """
        if self.fp:
            try:
                if verify_file:
                    self._handle_close_quarantine()
            except (Exception, Timeout), e:
                self.logger.error(_('ERROR DiskFile %(data_file)s in '
                     '%(data_dir)s close failure: %(exc)s : %(stack)'),
                     {'exc': e, 'stack': ''.join(traceback.format_stack()),
                      'data_file': self.data_file, 'data_dir': self.datadir})
            finally:
                self.fp.close()
                self.fp = None

    def mkstemp_nocontext(self):
        """
        Method to create a temporary file.
        Please note this temp file will not
        be automatically cleaned up, this must
        be done manually
        """
        if not os.path.exists(self.tmpdir):
            mkdirs(self.tmpdir)
        return mkstemp(dir=self.tmpdir)

    @contextmanager
    def mkstemp(self):
        """Contextmanager to make a temporary file."""
        if not os.path.exists(self.tmpdir):
            mkdirs(self.tmpdir)
        fd, tmppath = mkstemp(dir=self.tmpdir)
        try:
            yield fd, tmppath
        finally:
            try:
                os.close(fd)
            except OSError:
                pass
            try:
                os.unlink(tmppath)
            except OSError:
                pass

    def put(self, fd, tmppath, metadata):
        """
        Finalize writing the file on disk, and renames it from the temp file to
        the real location.  This should be called after the data has been
        written to the temp file.

        :params fd: file descriptor of the temp file
        :param tmppath: path to the temporary file being used
        :param metadata: dictionary of metadata to be written
        """
        metadata['name'] = self.name
        write_metadata(fd, metadata)
        if 'Content-Length' in metadata:
            self.drop_cache(fd, 0, int(metadata['Content-Length']))
        tpool.execute(os.fsync, fd)

        image_path = os.path.join(self.datadir, CHUNK_IMAGE_FILE)
        mkdirs(self.datadir)
        with lock_path(self.datadir):
            # Acquire lock on chunk path to avoid race conditions
            # when two concurrent requests are trying to create
            # the same same chunk

            if not os.path.exists(image_path):
                # Atomically rename to final dest and release lock
                renamer(tmppath, image_path)

        self.metadata = metadata

    def drop_cache(self, fd, offset, length):
        """Method for no-oping buffer cache drop method."""
        if not self.keep_cache:
            drop_buffer_cache(fd, offset, length)

    def is_deleted(self):
        """
        Check if the file is deleted.

        :returns: True if the file doesn't exist or has been flagged as
                  deleted.
        """
        return not self.data_file or 'deleted' in self.metadata

    def add_reference(self, reference):
        with lock_file(self.meta_file, append=True, unlink=False) as f:
            cPickle.dump(reference, f)

    def quarantine(self):
        """
        In the case that a file is corrupted, move it to a quarantined
        area to allow replication to fix it.

        :returns: if quarantine is successful, path to quarantined
                  directory otherwise None
        """
        if not (self.is_deleted() or self.quarantined_dir):
            self.quarantined_dir = quarantine_renamer(self.device_path,
                                                      self.data_file,
                                                      datadir='chunks')
            return self.quarantined_dir

    def get_data_file_size(self):
        """
        Returns the os.path.getsize for the file.  Raises an exception if this
        file does not match the Content-Length stored in the metadata. Or if
        self.data_file does not exist.

        :returns: file size as an int
        :raises DiskFileError: on file size mismatch.
        :raises DiskFileNotExist: on file not existing (including deleted)
        """
        try:
            file_size = 0
            if self.data_file:
                file_size = os.path.getsize(self.data_file)
                if 'Content-Length' in self.metadata:
                    metadata_size = int(self.metadata['Content-Length'])
                    if file_size != metadata_size:
                        raise DiskFileError('Content-Length of %s does not '
                          'match file size of %s' % (metadata_size, file_size))
                return file_size
        except OSError, err:
            if err.errno != errno.ENOENT:
                raise
        raise DiskFileNotExist('Data file does not exist.')


class ChunkController(BaseController):
    """Implements the WSGI application for the Swift Chunk Server."""
    server_type = _('Chunk')

    def __init__(self, conf):
        """
        Creates a new WSGI application for the Swift Chunk Server. An
        example configuration is given at
        <source-dir>/etc/chunk-server.conf-sample or
        /etc/swift/chunk-server.conf-sample.
        """
        BaseController.__init__(self, conf, 'chunk-server')
        self.chunk_ring = Ring(self.swift_dir, ring_name='chunk')
        self.devices = conf.get('devices', '/srv/node/')
        self.mount_check = conf.get('mount_check', 'true').lower() in \
                              TRUE_VALUES
        self.disk_chunk_size = int(conf.get('disk_chunk_size', 65536))
        self.network_chunk_size = int(conf.get('network_chunk_size', 65536))
        self.log_requests = conf.get('log_requests', 't') in TRUE_VALUES
        self.max_upload_time = int(conf.get('max_upload_time', 86400))
        self.slow = int(conf.get('slow', 0))
        self.fingerprint_algo = conf.get('fingerprint_algo', 'sha1')
        self.max_obj_mem_size = conf.get('max_obj_mem_size', 10240)
        try:
            self.hashalgo = HASH_ALGOS[self.fingerprint_algo]
        except KeyError:
            sys.exit("Fingerprint %s not supported." % self.fingerprint_algo)
        self.bytes_per_sync = int(conf.get('mb_per_sync', 512)) * 1024 * 1024
        default_allowed_headers = '''
            content-disposition,
            content-encoding,
        '''
        self.allowed_headers = set(i.strip().lower() for i in \
                conf.get('allowed_headers', \
                default_allowed_headers).split(',') if i.strip() and \
                i.strip().lower() not in DISALLOWED_HEADERS)

    def PUT(self, req):
        """
        Handle HTTP PUT requests for the Swift Chunk Server.

        PUT will store a chunk locally and replicate it to the
        correct set of servers
        """

        req.headers['X-Timestamp'] = normalize_timestamp(time.time())

        try:
            device, partition, account, container, obj, chunk_id = \
                split_path(unquote(req.path), 6, 6)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=req,
                        content_type='text/plain')

        error_response = check_chunk_put(req, chunk_id) or \
                            check_fingerprint(req, chunk_id,
                                                     self.fingerprint_algo)
        # If headers are wrong, no need to forward request to other servers
        if error_response:
            return error_response

        return self.do_put(req, chunk_id)

    def do_put(self, req, chunk_id, read_func=None, fd=None, tmppath=None):

        # Get nodes responsible for storing this chunk in the ring
        partition, nodes = self.chunk_ring.get_chunk_nodes(chunk_id)
        shuffle(nodes)
        replicas = len(nodes)
        quorum = replicas // 2 + 1
        local_store = False
        device = None
        nodes, devices = self.remove_myself(nodes)
        if len(devices) == 1:
            local_store = True
            quorum -= 1
            device = devices[0]
        elif len(devices) > 1:
            return HTTPServerError(request=req, body='ERROR: Two replicas'
                                   ' for one object in the same node',
                                   content_type='text/plain')

        this_response = None
        if local_store:
                if self.mount_check and not check_mount(self.devices, device):
                    # TODO: chunk server must still proxy the request
                    # to other servers if the device is not mounted
                    this_response = HTTPInsufficientStorage(device, request=req)
                    local_store = False
                else:
                    chunk_file = ChunkFile(self.devices, device, partition,
                                           chunk_id, self.logger,
                                        disk_chunk_size=self.disk_chunk_size,
                                        keep_data_fp=True)

                    if not chunk_file.is_deleted():
                        # Chunk already exists. Send success response.
                        # TODO: Should this server still proxy request to other servers to
                        # fix replication errors?
                        if 'X-Chunk-Obj-Id' in req.headers:
                            chunk_file.add_reference(req.headers['X-Chunk-Obj-Id'])
                        this_response = HTTPOk(request=req, etag=chunk_file.metadata['ETag'])
                        local_store = False

        # Connect to other nodes
        node_iter = self.iter_nodes(partition, nodes, self.chunk_ring)
        conns = self.connect_chunk_servers(req, partition, node_iter,
                                            len(nodes))

        self.check_quorum("Chunk PUT", req, len(conns), quorum)

        other_responses, conns = self.get_responses(conns)
        quorum -= len(other_responses)

        try:
            if not read_func:
                read_func = req.environ['wsgi.input'].read

            data_source = iter(lambda: read_func(self.network_chunk_size), '')

            disk_write = False
            if local_store and not fd and not tmppath:
                disk_write = True
                fd, tmppath = chunk_file.mkstemp_nocontext()
                if 'content-length' in req.headers:
                    fallocate(fd, int(req.headers['content-length']))

            if local_store or conns:
                conns, etag = self.cut_through_replication(req, chunk_id, conns,
                                                       fd,  tmppath, quorum,
                                                       data_source, disk_write)

            if local_store:
                try:
                    this_response = self.finish_chunk_store(req, etag,
                                                            chunk_file,
                                                            fd, tmppath)
                except Exception:
                    self.logger.exception(_('ERROR while writing request'
                                            ' to disk: ' + req.path))
                    this_response = HTTPServerError(request=req)
        finally:
            if local_store:
                #Clean-up temp file
                try:
                    os.close(fd)
                except OSError:
                    pass
                try:
                    os.unlink(tmppath)
                except OSError:
                    pass

        other_responses.extend(self.collect_responses(req, conns,
                                                'Chunk PUT'))

        return self.generate_response(req, this_response, other_responses,
                                     replicas)

    def STORE(self, request):
        """
        Handle HTTP STORE requests for the Swift Chunk Server.

        STORE requests locally store a chunk in a chunk server,
        if it does not yet exist.

        This operation is not responsible for replicating
        chunks to the correct servers.
        """

        try:
            device, partition, account, container, obj, chunk_id = \
                split_path(unquote(request.path), 6, 6)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=request,
                        content_type='text/plain')

        if self.mount_check and not check_mount(self.devices, device):
            return HTTPInsufficientStorage(device, request=request)

        # Verify if required headers and fingerprint are correct
        error_response = check_chunk_store(request, chunk_id) or \
                            check_fingerprint(request, chunk_id,
                                                     self.fingerprint_algo)
        if error_response:
            return error_response

        chunk_file = ChunkFile(self.devices, device, partition,
                               chunk_id, self.logger,
                               disk_chunk_size=self.disk_chunk_size,
                               keep_data_fp=True)

        if not chunk_file.is_deleted():
            if 'X-Chunk-Obj-Id' in request.headers:
                chunk_file.add_reference(request.headers['X-Chunk-Obj-Id'])
            # Chunk already exists. Send success response.
            return HTTPOk(request=request, etag=chunk_file.metadata['ETag'])

        upload_expiration = time.time() + self.max_upload_time
        etag = md5()
        fingerprint = self.hashalgo()
        upload_size = 0
        last_sync = 0
        # Receive chunk payload
        with chunk_file.mkstemp() as(fd, tmppath):
            if 'content-length' in request.headers:
                fallocate(fd, int(request.headers['content-length']))
            reader = request.environ['wsgi.input'].read
            for chunk in iter(lambda: reader(self.network_chunk_size), ''):
                upload_size += len(chunk)
                if time.time() > upload_expiration:
                    return HTTPRequestTimeout(request=request)
                etag.update(chunk)
                fingerprint.update(chunk)
                while chunk:
                    written = os.write(fd, chunk)
                    chunk = chunk[written:]
                # For large chunks sync every 512MB (by default) written
                if upload_size - last_sync >= self.bytes_per_sync:
                    tpool.execute(os.fdatasync, fd)
                    drop_buffer_cache(fd, last_sync, upload_size - last_sync)
                    last_sync = upload_size

            etag = etag.hexdigest()
            # TODO: Currently fingerprint check is done during STORE,
            # but in the future this check should only be done during a PUT.
            self.check_uploaded_content_length(request, upload_size)
            self.check_etag(request, etag)
            self.check_fingerprint(request, fingerprint.hexdigest(),
                                                                    chunk_id)

            return self.finish_chunk_store(request, etag, chunk_file, fd,
                                            tmppath)

    def DEDUP_PUT(self, req):
        """
        Handle HTTP DEDUP_PUT requests for the Swift Chunk Server.
        """

        req.headers['X-Timestamp'] = normalize_timestamp(time.time())

        if req.content_length is None:
            if req.headers.get('transfer-encoding') != 'chunked':
                return HTTPLengthRequired(request=req)
        else:
            if req.content_length <= 0:
                return HTTPBadRequest(body='Request must have positive'
                                  ' length.', request=req,
                                  content_type='text/plain')

        if req.content_length > MAX_FILE_SIZE:
            return HTTPRequestEntityTooLarge(body='Your request is too large.',
                                       request=req, content_type='text/plain')

        try:
            device, partition, object, account, container = \
                                    split_path(unquote(req.path), 2, 5)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=req,
                        content_type='text/plain')

        if self.mount_check and not check_mount(self.devices, device):
            return HTTPInsufficientStorage(device, request=req)

        write_buffer = None
        fd, tmppath = None, None
        tmp_store = False
        if req.content_length is None or \
                    int(req.content_length) > self.max_obj_mem_size:
            tmp_store = True
            self.tmpdir = os.path.join(self.devices, device, 'tmp')
            if not os.path.exists(self.tmpdir):
                mkdirs(self.tmpdir)
            fd, tmppath = mkstemp(dir=self.tmpdir)
            write_buffer = os.fdopen(fd, "w")
        else:
            write_buffer = cStringIO.StringIO()

        upload_expiration = time.time() + self.max_upload_time
        etag = md5()
        fingerprint = self.hashalgo()
        upload_size = 0
        last_sync = 0
        bufval = ''
        # Receive chunk payload
        try:
            reader = req.environ['wsgi.input'].read
            for chunk in iter(lambda: reader(self.network_chunk_size), ''):
                upload_size += len(chunk)
                if time.time() > upload_expiration:
                    return HTTPRequestTimeout(request=req)
                etag.update(chunk)
                fingerprint.update(chunk)
                write_buffer.write(chunk)
                # For large chunks sync every 512MB (by default) written
                if tmp_store and upload_size - last_sync >= self.bytes_per_sync:
                    tpool.execute(os.fdatasync, write_buffer.fileno())
                    drop_buffer_cache(write_buffer.fileno(), last_sync,
                                      upload_size - last_sync)
                    last_sync = upload_size
            if not tmp_store:
                bufval = write_buffer.getvalue()
        finally:
            try:
                write_buffer.close()
            except OSError:
                pass

        try:
            read_buffer = None
            if tmp_store:
                read_buffer = open(tmppath)
            else:
                read_buffer = cStringIO.StringIO(bufval)

            etag = etag.hexdigest()
            # TODO: Currently fingerprint check is done during STORE,
            # but in the future this check should only be done during a PUT.
            self.check_uploaded_content_length(req, upload_size)
            self.check_etag(req, etag)
            chunk_id = fingerprint.hexdigest()

            req.path_info += chunk_id
            resp = self.do_put(req, chunk_id, read_buffer.read, fd, tmppath)

            if is_success(resp.status_int):
                headers = resp.headers
                headers['X-Chunk-Id'] = chunk_id
                resp.headers = headers

            return resp
        finally:
            try:
                write_buffer.close()
            except OSError:
                pass

	    if tmp_store:
                try:
                    os.close(fd)
                except OSError:
                    pass
                try:
                    os.unlink(tmppath)
                except OSError:
                    pass

    def check_fingerprint(self, req, fingerprint, chunk_id):
        if fingerprint != chunk_id:
            # Not sure if this is the best error code to return
            raise HTTPUnprocessableEntity(request=req,
                                           body='Chunk fingerprint does not '
                                                'match with data %s digest.' %
                                                    self.fingerprint_algo,
                                           content_type='text/plain')

    def finish_chunk_store(self, req, etag, data_file, fd, tmppath):
        file_size = os.fstat(fd).st_size

        self.check_stored_content_length(req, tmppath, file_size)

        metadata = {'X-Timestamp': req.headers['x-timestamp'],
                    'ETag': etag,
                    'Content-Length': str(file_size)
        }

        #TODO: Eventually add x-chunk-meta- attributes, if there is any
        for header_key in self.allowed_headers:
            if header_key in req.headers:
                header_caps = header_key.title()
                metadata[header_caps] = req.headers[header_key]

        # TODO: add session hold back-reference
        data_file.put(fd, tmppath, metadata)

        if 'X-Chunk-Obj-Id' in req.headers:
            data_file.add_reference(req.headers['X-Chunk-Obj-Id'])

        return HTTPCreated(request=req, etag=etag)

    def cut_through_replication(self, req, chunk_id, conns, fd,
                                 tmppath, quorum, data_source,
                                 disk_write=True):
        num_threads = len(conns) + int(disk_write)
        etag = md5()
        fingerprint = self.hashalgo()
        with ContextPool(num_threads) as pool:
            # Start connection threads
            for conn in conns:
                conn.failed = False
                conn.queue = Queue(self.transfer_queue_depth)
                pool.spawn(self.send_file, conn, req.path)

            write_queue = None
            if disk_write:
                # Start disk write thread
                write_queue = Queue(self.disk_queue_depth)
                pool.spawn(self.write_file, write_queue, fd,
                                            tmppath, req.path)

            try:
                conns = self.pipeline_data(req, data_source, \
                            conns, quorum, write_queue, etag, fingerprint)
            except WSGIHTTPException, exc:
                raise exc
            except (Exception, Timeout):
                req.client_disconnect = True
                self.logger.exception(
                    _('ERROR Exception causing client disconnect'))
                raise HTTPClientDisconnect(request=req)

            # Wait for connection threads to finish sending data
            [conn.queue.join() for conn in conns
                                        if conn.queue.unfinished_tasks]

            # Wait for disk write thread to finish writing data
            if write_queue:
                write_queue.join()

        # TODO: In the future, fingerprint validation must be done only
        # in PUT, not in STORE.
        # In order to enable this, Chunk Server must only forward the 
        # last piece of data to the remaining servers if the fingerprint 
        # was validated correctly, otherwise the connection must be closed.
        etag = etag.hexdigest()
        self.check_uploaded_content_length(req, req.bytes_transferred)
        self.check_etag(req, etag)
        self.check_fingerprint(req, fingerprint.hexdigest(),
                                                      chunk_id)

        return conns, etag

    def pipeline_data(self, req, data_source, conns, quorum,
                                 write_queue, etag, fingerprint):
        # Start reading Chunks from wsgi data source
        req.bytes_transferred = 0
        chunked = req.headers.get('transfer-encoding')
        upload_expiration = time.time() + self.max_upload_time
        while True:
            try:
                with ChunkReadTimeout(self.client_timeout):
                    # Read  next Chunk from input source
                    chunk = next(data_source)
            except StopIteration:
                if chunked:
                    [conn.queue.put('0\r\n\r\n') for conn in conns]
                return conns
            except ChunkReadTimeout, err:
                self.logger.warn(_('ERROR Client read timeout (%ss)'),
                                 err.seconds)
                raise HTTPRequestTimeout(request=req)

            # Verify constraints
            if time.time() > upload_expiration:
                raise HTTPRequestTimeout(request=req)

            req.bytes_transferred += len(chunk)
            if req.bytes_transferred > MAX_FILE_SIZE:
                raise HTTPRequestEntityTooLarge(request=req)

            # Replicate chunk to open connections
            for conn in list(conns):
                if not conn.failed:
                    conn.queue.put('%x\r\n%s\r\n' % (len(chunk), chunk)
                                    if chunked else chunk)
                else:
                    conns.remove(conn)

            # Verify if still has quorum of connections
            self.check_quorum("Chunk PUT", req, len(conns), quorum)

            # Add chunk to disk write queue
            if write_queue:
                write_queue.put(chunk)

            # Update etag and fingerprint hash objects
            # with chunk data
            etag.update(chunk)
            fingerprint.update(chunk)

    def POST(self, request):
        """Handle HTTP POST requests for the Swift Object Server."""
        try:
            device, partition, account, container, obj, chunk_id = \
                split_path(unquote(request.path), 6, 6)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=request,
                        content_type='text/plain')
        if 'x-timestamp' not in request.headers or \
                    not check_float(request.headers['x-timestamp']):
            return HTTPBadRequest(body='Missing timestamp', request=request,
                        content_type='text/plain')

        if self.mount_check and not check_mount(self.devices, device):
            return HTTPInsufficientStorage(device, request=request)

        chunk_file = ChunkFile(self.devices, device, partition,
                               chunk_id, self.logger,
                               disk_chunk_size=self.disk_chunk_size,
                               read_meta=False)

        if chunk_file.is_deleted():
                return HTTPNotFound(request=request)

        if 'X-Chunk-Obj-Id' in request.headers:
            chunk_file.add_reference(request.headers['X-Chunk-Obj-Id'])

        return HTTPNoContent(request=request)

    def GET(self, request):
        """Handle HTTP GET requests for the Swift Chunk Server."""
        try:
            device, partition, account, container, obj, chunk_id = \
                split_path(unquote(request.path), 6, 6)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=request,
                        content_type='text/plain')

        if self.mount_check and not check_mount(self.devices, device):
            return HTTPInsufficientStorage(device, request=request)

        chunk_file = ChunkFile(self.devices, device, partition,
                               chunk_id, self.logger,
                               disk_chunk_size=self.disk_chunk_size,
                               keep_data_fp=True)

        if chunk_file.is_deleted():
                return HTTPNotFound(request=request)

        try:
            chunk_size = chunk_file.get_data_file_size()
        except (DiskFileError, DiskFileNotExist):
            chunk_file.quarantine()
            return HTTPNotFound(request=request)

        #TODO: Not sure if we should support conditional responses
        # for chunks, since they are immutable. Supporting for now.
        if request.headers.get('if-match') not in (None, '*') and \
                chunk_file.metadata['ETag'] not in request.if_match:
            chunk_file.close()
            return HTTPPreconditionFailed(request=request)
        if request.headers.get('if-none-match') is not None:
            if chunk_file.metadata['ETag'] in request.if_none_match:
                resp = HTTPNotModified(request=request)
                resp.etag = chunk_file.metadata['ETag']
                chunk_file.close()
                return resp
        try:
            if_unmodified_since = request.if_unmodified_since
        except (OverflowError, ValueError):
            # catches timestamps before the epoch
            return HTTPPreconditionFailed(request=request)
        if if_unmodified_since and \
           datetime.fromtimestamp(float(file.metadata['X-Timestamp']), UTC) > \
           if_unmodified_since:
            chunk_file.close()
            return HTTPPreconditionFailed(request=request)
        try:
            if_modified_since = request.if_modified_since
        except (OverflowError, ValueError):
            # catches timestamps before the epoch
            return HTTPPreconditionFailed(request=request)
        if if_modified_since and \
           datetime.fromtimestamp(float(file.metadata['X-Timestamp']), UTC) < \
           if_modified_since:
            chunk_file.close()
            return HTTPNotModified(request=request)

        # TODO: Not sure if all headers should be in the response, or only the
        # payload. Leaving headers for now.
        response = Response(app_iter=chunk_file, request=request,
                            conditional_response=True)
        response.headers['Content-Type'] = 'application/octet-stream'
        response.etag = chunk_file.metadata['ETag']
        response.content_length = chunk_size
        response.last_modified = float(chunk_file.metadata['X-Timestamp'])

        if 'Content-Encoding' in chunk_file.metadata:
            response.content_encoding = chunk_file.metadata['Content-Encoding']

        if response.content_length < KEEP_CACHE_SIZE and \
                'X-Auth-Token' not in request.headers and \
                'X-Storage-Token' not in request.headers:
            chunk_file.keep_cache = True

        for key, value in chunk_file.metadata.iteritems():
            if key.lower().startswith('x-chunk-meta-') or \
                    key.lower() in self.allowed_headers:
                response.headers[key] = value

        return request.get_response(response)

    def HEAD(self, request):
        """Handle HTTP HEAD requests for the Swift Chunk Server."""
        try:
            device, partition, account, container, obj, chunk_id = \
                split_path(unquote(request.path), 6, 6)
        except ValueError, err:
            return HTTPBadRequest(body=str(err), request=request,
                        content_type='text/plain')

        if self.mount_check and not check_mount(self.devices, device):
            return HTTPInsufficientStorage(device, request=request)

        chunk_file = ChunkFile(self.devices, device, partition,
                               chunk_id, self.logger,
                               disk_chunk_size=self.disk_chunk_size)

        if chunk_file.is_deleted():
                return HTTPNotFound(request=request)

        try:
            chunk_size = chunk_file.get_data_file_size()
        except (DiskFileError, DiskFileNotExist):
            chunk_file.quarantine()
            return HTTPNotFound(request=request)

        response = Response(request=request, conditional_response=True)
        response.headers['Content-Type'] = 'application/octet-stream'
        response.etag = chunk_file.metadata['ETag']
        response.content_length = chunk_size
        response.last_modified = float(chunk_file.metadata['X-Timestamp'])

        if 'Content-Encoding' in chunk_file.metadata:
            response.content_encoding = chunk_file.metadata['Content-Encoding']

        if response.content_length < KEEP_CACHE_SIZE and \
                'X-Auth-Token' not in request.headers and \
                'X-Storage-Token' not in request.headers:
            chunk_file.keep_cache = True

        for key, value in chunk_file.metadata.iteritems():
            if key.lower().startswith('x-chunk-meta-') or \
                    key.lower() in self.allowed_headers:
                response.headers[key] = value

        return response

    def connect_chunk_server(self, method, nodes, part, path, headers):
        """Method for connecting to a chunk server"""
        for node in nodes:
            try:
                with ConnectionTimeout(self.conn_timeout):
                    conn = http_connect(node['ip'], node['port'],
                            node['device'], part, method, path, headers)
                with Timeout(self.node_timeout):
                    resp = conn.getexpect()
                if resp.status == 100:
                    # Return connection, so data can be sent
                    conn.response = None
                    conn.node = node
                    return conn
                if resp.status == 200:
                    # Chunk was already stored,
                    # return connection and response
                    conn.response = resp
                    conn.node = node
                    return conn
                elif resp.status == 507:
                    self.error_limit(node)
                else:
                    self.exception_occurred(node, self.server_type,
                        _('Expect: 100-continue on %s, and got '
                          '%d') % (path, resp.status))
            except:
                self.exception_occurred(node, self.server_type,
                    _('Expect: 100-continue on %s') % path)

    def get_responses(self, conns):
        """
        Retrieves "expect 100 continue" responses from the given connections,
        if there are any, and also return the updated list of connections

        :param conns: open connections
        """
        new_conns = []
        nodes_responses = []

        for conn in conns:
            if conn.response:
                nodes_responses.append(conn.response)
                conn.close()
            else:
                new_conns.append(conn)

        return nodes_responses, new_conns

    def connect_chunk_servers(self, req, partition, nodes, replicas):
        # remove device and partition prefix from
        # request path (to connect to next nodes)
        req.path_info_pop()
        req.path_info_pop()

        pile = GreenPile(replicas)
        for i in range(replicas):
            nheaders = dict(req.headers.iteritems())
            nheaders['Connection'] = 'close'
            nheaders['Expect'] = '100-continue'
            pile.spawn(self.connect_chunk_server, 'STORE', nodes,
                       partition, req.path_info, nheaders)
        conns = [conn for conn in pile if conn]
        return conns

    def __call__(self, env, start_response):
        """WSGI Application entry point for the Swift Object Server."""
        start_time = time.time()
        req = Request(env)
        self.logger.txn_id = req.headers.get('x-trans-id', None)
        if not check_utf8(req.path_info):
            res = HTTPPreconditionFailed(body='Invalid UTF8')
        else:
            try:
                if hasattr(self, req.method):
                    res = getattr(self, req.method)(req)
                else:
                    res = HTTPMethodNotAllowed()
            except WSGIHTTPException, exc:
                # WSGIHTTPException is also a Response
                res = exc
            except (Exception, Timeout):
                self.logger.exception(_('ERROR __call__ error with %(method)s'
                    ' %(path)s '), {'method': req.method, 'path': req.path})
                res = HTTPInternalServerError(body=traceback.format_exc())
        trans_time = time.time() - start_time
        if self.log_requests:
            log_line = '%s - - [%s] "%s %s" %s %s "%s" "%s" "%s" %.4f' % (
                req.remote_addr,
                time.strftime('%d/%b/%Y:%H:%M:%S +0000',
                              time.gmtime()),
                req.method, req.path, res.status.split()[0],
                res.content_length or '-', req.referer or '-',
                req.headers.get('x-trans-id', '-'),
                req.user_agent or '-',
                trans_time)
            self.logger.info(log_line)
        if req.method in ('PUT'):
            slow = self.slow - trans_time
            if slow > 0:
                sleep(slow)
        return res(env, start_response)


def app_factory(global_conf, **local_conf):
    """paste.deploy app factory for creating WSGI object server apps"""
    conf = global_conf.copy()
    conf.update(local_conf)
    return ChunkController(conf)
