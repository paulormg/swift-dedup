# Copyright (c) 2010-2012 OpenStack, LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" Tests for swift.chunk_server """

import cPickle as pickle
import os
import sys
import shutil
import unittest
from nose import SkipTest
from shutil import rmtree
from StringIO import StringIO
from time import gmtime, sleep, strftime, time
from tempfile import mkdtemp
from hashlib import md5
from gzip import GzipFile

from eventlet import sleep, spawn, wsgi, listen, Timeout
from webob import Request
from webob.exc import WSGIHTTPException

from test.unit import FakeLogger
from test.unit import _getxattr as getxattr
from test.unit import _setxattr as setxattr
from test.unit import connect_tcp, readuntil2crlfs

from swift.common import ring
from swift.chunk import server as chunk_server
from swift.common import utils
from swift.common.utils import hash_path, mkdirs, normalize_timestamp, \
                               NullLogger, storage_directory
from swift.common.exceptions import DiskFileNotExist
from swift.obj import replicator
from eventlet import tpool


class TestChunkFile(unittest.TestCase):
    """Test swift.chunk.server.ChunkFile"""

    def setUp(self):
        """ Set up for testing swift.chunk.server.ChunkController """
        self.testdir = os.path.join(mkdtemp(), 'tmp_test_chk_server_ChunkFile')
        mkdirs(os.path.join(self.testdir, 'sda1', 'tmp'))

        self.execute = tpool.execute

        def fake_exe(*args, **kwargs):
            pass
        tpool.execute = fake_exe

    def tearDown(self):
        """ Tear down for testing swift.chunk.server.ChunkController """
        rmtree(os.path.dirname(self.testdir))
        tpool.execute = self.execute

    def test_chunk_file_exists(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=False)

        self.assertTrue(cf.is_deleted(),
                         "It's a new chunk, it should not be present.")

        # Create the chunk manually
        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_path = storage_directory(datadir, partition, chunk_id)
        mkdirs(chunk_path)
        f = open(os.path.join(chunk_path,
                              chunk_server.CHUNK_IMAGE_FILE), 'wb')

        f.write('1234567890')
        setxattr(f.fileno(), chunk_server.METADATA_KEY,
                 pickle.dumps({}, chunk_server.PICKLE_PROTOCOL))
        f.close()

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=False)

        self.assertFalse(cf.is_deleted(),
                        "Chunk was created manually, it should be present.")

    def test_chunk_file_put(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=False)

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_path = storage_directory(datadir, partition, chunk_id)

        metadata = {"k1": "v1", "k2": "v2"}

        #write to file, put and check if tmpfile was renamed to final name
        with cf.mkstemp() as (fd, tmppath):
            os.write(fd, '1234567890')
            self.assertFalse(os.path.exists(chunk_path), "Chunk file"
                                            "should not be created")
            cf.put(fd, tmppath, metadata)
            self.assertTrue(os.path.exists(chunk_path), "Chunk file"
                                            "should be created")

        cf2 = chunk_server.ChunkFile(self.testdir, device, partition,
                                     chunk_id, FakeLogger(),
                                     keep_data_fp=True)

        self.assertTrue(not cf2.is_deleted())

        #check if metadata was written
        final_metadata = {"k1": "v1", "k2": "v2", "name": "/" + chunk_id}
        self.assertEquals(cf2.metadata, final_metadata)

    def test_chunk_file_app_iter_corners(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=False)
        mkdirs(cf.datadir)

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition,
                                  chunk_id), chunk_server.CHUNK_IMAGE_FILE)
        f = open(chunk_file, 'wb')
        f.write('1234567890')
        setxattr(f.fileno(), chunk_server.METADATA_KEY,
                 pickle.dumps({}, chunk_server.PICKLE_PROTOCOL))
        f.close()

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=True)
        it = cf.app_iter_range(0, None)
        sio = StringIO()
        for chunk in it:
            sio.write(chunk)
        self.assertEquals(sio.getvalue(), '1234567890')

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=True)
        it = cf.app_iter_range(5, None)
        sio = StringIO()
        for chunk in it:
            sio.write(chunk)
        self.assertEquals(sio.getvalue(), '67890')

    def test_disk_file_mkstemp_creates_dir(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'

        tmpdir = os.path.join(self.testdir, device, 'tmp')
        os.rmdir(tmpdir)
        with chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger()).mkstemp():
            self.assert_(os.path.exists(tmpdir))

    def test_quarantine(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())
        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition,
                                  chunk_id), chunk_server.CHUNK_IMAGE_FILE)
        mkdirs(cf.datadir)
        f = open(chunk_file, 'wb')
        setxattr(f.fileno(), chunk_server.METADATA_KEY,
                 pickle.dumps({}, chunk_server.PICKLE_PROTOCOL))

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())
        cf.quarantine()
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                                os.path.basename(os.path.dirname(
                                                            cf.data_file)))
        self.assert_(os.path.isdir(quar_dir))

    def test_quarantine_same_file(self):
        device = 'sda1'
        partition = '0'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition,
                                  chunk_id), chunk_server.CHUNK_IMAGE_FILE)
        mkdirs(cf.datadir)
        f = open(chunk_file, 'wb')
        setxattr(f.fileno(), chunk_server.METADATA_KEY,
                 pickle.dumps({}, chunk_server.PICKLE_PROTOCOL))

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())
        new_dir = cf.quarantine()
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                                os.path.basename(os.path.dirname(
                                                            cf.data_file)))
        self.assert_(os.path.isdir(quar_dir))
        self.assertEquals(quar_dir, new_dir)
        # have to remake the datadir and file
        mkdirs(cf.datadir)
        f = open(chunk_file, 'wb')
        setxattr(f.fileno(), chunk_server.METADATA_KEY,
                 pickle.dumps({}, chunk_server.PICKLE_PROTOCOL))

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())
        double_uuid_path = cf.quarantine()
        self.assert_(os.path.isdir(double_uuid_path))
        self.assert_('-' in os.path.basename(double_uuid_path))

    def _get_data_file(self, invalid_type=None, chunk_id='188a5356a091db746' \
                       '5c484a0e8ccd23b6d015b10a956128851591082116a240b',
                       fsize=1024, csize=8, ts=None):
        '''returns a DiskFile'''

        device = 'sda1'
        partition = '0'
        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger())
        data = '0' * fsize
        etag = md5()
        if ts:
            timestamp = ts
        else:
            timestamp = str(normalize_timestamp(time()))

        with cf.mkstemp() as (fd, tmppath):
            os.write(fd, data)
            etag.update(data)
            etag = etag.hexdigest()
            metadata = {
                'ETag': etag,
                'X-Timestamp': timestamp,
                'Content-Length': str(os.fstat(fd).st_size),
            }
            cf.put(fd, tmppath, metadata)
            if invalid_type == 'ETag':
                etag = md5()
                etag.update('1' + '0' * (fsize - 1))
                etag = etag.hexdigest()
                metadata['ETag'] = etag
                chunk_server.write_metadata(fd, metadata)
            if invalid_type == 'Content-Length':
                metadata['Content-Length'] = fsize - 1
                chunk_server.write_metadata(fd, metadata)

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                    FakeLogger(), keep_data_fp=True,
                                    disk_chunk_size=csize)

        if invalid_type == 'Zero-Byte':
            os.remove(cf.data_file)
            fp = open(cf.data_file, 'w')
            fp.close()
        cf.unit_test_len = fsize
        return cf

    def test_quarantine_valids(self):
        cf = self._get_data_file(chunk_id='1')
        for chunk in cf:
            pass
        self.assertFalse(cf.quarantined_dir)

        cf = self._get_data_file(chunk_id='2', csize=1)
        for chunk in cf:
            pass
        self.assertFalse(cf.quarantined_dir)

        cf = self._get_data_file(chunk_id='3', csize=100000)
        for chunk in cf:
            pass
        self.assertFalse(cf.quarantined_dir)

    def run_quarantine_invalids(self, invalid_type):
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='1')
        for chunk in cf:
            pass
        self.assertTrue(cf.quarantined_dir)
        cf = self._get_data_file(invalid_type=invalid_type,
                                 chunk_id='2', csize=1)
        for chunk in cf:
            pass
        self.assertTrue(cf.quarantined_dir)
        cf = self._get_data_file(invalid_type=invalid_type,
                                 chunk_id='3', csize=100000)
        for chunk in cf:
            pass
        self.assertTrue(cf.quarantined_dir)
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='4')
        self.assertFalse(cf.quarantined_dir)
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='5')
        for chunk in cf.app_iter_range(0, cf.unit_test_len):
            pass
        self.assertTrue(cf.quarantined_dir)
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='6')
        for chunk in cf.app_iter_range(0, cf.unit_test_len + 100):
            pass
        self.assertTrue(cf.quarantined_dir)
        expected_quar = False
        #for the following, Content-Length/Zero-Byte errors will always result
        #in a quarantine, even if the whole file isn't check-summed
        if invalid_type in ('Zero-Byte', 'Content-Length'):
            expected_quar = True
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='7')
        for chunk in cf.app_iter_range(1, cf.unit_test_len):
            pass
        self.assertEquals(bool(cf.quarantined_dir), expected_quar)
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='8')
        for chunk in cf.app_iter_range(0, cf.unit_test_len - 1):
            pass
        self.assertEquals(bool(cf.quarantined_dir), expected_quar)
        cf = self._get_data_file(invalid_type=invalid_type, chunk_id='8')
        for chunk in cf.app_iter_range(1, cf.unit_test_len + 1):
            pass
        self.assertEquals(bool(cf.quarantined_dir), expected_quar)

    def test_quarantine_invalid_etag(self):
        self.run_quarantine_invalids('ETag')

    def test_quarantine_invalid_content_length(self):
        self.run_quarantine_invalids('Content-Length')

    def test_quarantine_invalid_zero_byte(self):
        self.run_quarantine_invalids('Zero-Byte')

    def test_close_error(self):

        def err():
            raise Exception("bad")

        cf = self._get_data_file(fsize=1024 * 1024 * 2)
        cf._handle_close_quarantine = err
        for chunk in cf:
            pass
        #close is called at the end of the iterator
        self.assertEquals(cf.fp, None)
        self.assertEquals(len(cf.logger.log_dict['error']), 1)

    def test_quarantine_twice(self):
        cf = self._get_data_file(invalid_type='Content-Length')
        self.assert_(os.path.isfile(cf.data_file))
        quar_dir = cf.quarantine()
        self.assertFalse(os.path.isfile(cf.data_file))
        self.assert_(os.path.isdir(quar_dir))
        self.assertEquals(cf.quarantine(), None)


class TestChunkController(unittest.TestCase):
    """ Test swift.chunk.server.ChunkController """

    def setUp(self):
        """ Set up for testing swift.chunk.server.ChunkController """
        utils.HASH_PATH_SUFFIX = 'endcap'
        self.testdir = \
            os.path.join(mkdtemp(), 'tmp_test_chunk_server_ChunkController')
        mkdirs(os.path.join(self.testdir, 'sda1', 'tmp'))
        conf = {'swift_dir': self.testdir,
                'devices': self.testdir,
                'mount_check': 'false',
                'fingerprint_algo': 'sha256'}
        pickle.dump(ring.RingData([[0, 1, 0, 1], [1, 0, 1, 0]],
        [{'id': 0, 'zone': 0, 'device': 'sda1', 'ip': '127.0.0.1',
          'port': 0},
         {'id': 1, 'zone': 1, 'device': 'sdb1', 'ip': '127.0.0.1',
          'port': 0}], 30),
        GzipFile(os.path.join(self.testdir, 'chunk.ring.gz'), 'wb'))
        self.chunk_controller = chunk_server.ChunkController(conf)
        self.chunk_controller.bytes_per_sync = 1

    def tearDown(self):
        """ Tear down for testing swift.chunk.server.ChunkController """
        rmtree(os.path.dirname(self.testdir))

    def test_STORE_invalid_path(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b/a'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Length': '0'})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)

        url_prefix = '/sda1/0/acc/container/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Length': '0'})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)

        chunk_url = '/'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Length': '0'})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)

        chunk_url = ''
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp':
                                            normalize_timestamp(time()),
                         'Content-Length': '0'})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)

    def test_STORE_no_timestamp(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE'},
                headers={'Content-Length': '0'})

        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Missing timestamp')

    def test_STORE_no_content_type(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time())})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'No content type')

    def test_STORE_invalid_content_type(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Type': 'text/plain'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Chunk server only accepts'
                         ' application/octet-stream" content-type.')

    def test_STORE_no_content_length(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        del req.headers['Content-Length']
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 411)

    def test_STORE_invalid_content_length(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE',
                                                'CONTENT_LENGTH': '-1'},
                headers={'Content-Length': '-1',
                         'X-Timestamp': normalize_timestamp(time())})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Request must have positive length.')

        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE',
                                                'CONTENT_LENGTH': '0'},
                headers={'Content-Length': '-1',
                         'X-Timestamp': normalize_timestamp(time())})
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Request must have positive length.')

    def test_STORE_invalid_fingerprint(self):
        url_prefix = '/sda1/0/acc/container/obj/'

        # fingerprint too short (sha256 fingerprint must have 64 chars)
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Invalid sha256 chunk fingerprint.')

        # fingerprint with invalid character (*) - only alphanumerical
        # accepted
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240*'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url, environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 400)
        self.assertEquals(resp.body, 'Invalid sha256 chunk fingerprint.')

        # Invalid fingerprint (does not match data sha256 digest)
        chunk_id = '1111111111111111111111111111111111111111111111111111'\
                   '111111111111'
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': normalize_timestamp(time()),
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        try:
            self.chunk_controller.STORE(req)
            self.fail("should not reach here")
        except WSGIHTTPException, resp:
            self.assertEquals(resp.status_int, 422)
            self.assertEquals(resp.body, 'Chunk fingerprint does not match'
                                         ' with data sha256 digest.')

    def test_STORE_common(self):
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition, chunk_id),
                                  chunk_server.CHUNK_IMAGE_FILE)
        self.assert_(os.path.isfile(chunk_file))
        self.assertEquals(open(chunk_file).read(), 'VERIFY')

        metadata = pickle.loads(getxattr(chunk_file,
                            chunk_server.METADATA_KEY))
        self.assertEquals(metadata,
                          {'X-Timestamp': timestamp,
                           'Content-Length': '6',
                           'ETag': '0b4c12d7e0a73840c1c4f148fda3b037',
                           'name': '/188a5356a091db7465c484a0e8'
                            'ccd23b6d015b10a956128851591082116a240b'})

    def test_STORE_existing_chunk(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        timestamp = normalize_timestamp(time())
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)
        sleep(.00001)

        # 200 OK must be received (chunk already exists)
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 200)

    def test_STORE_invalid_etag(self):
        url_prefix = '/sda1/0/acc/container/obj/'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        timestamp = normalize_timestamp(time())
        chunk_url = url_prefix + chunk_id
        req = Request.blank(chunk_url,
                            environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream',
                         'ETag': 'invalid'})
        req.body = 'test'
        try:
            self.chunk_controller.STORE(req)
            self.fail("should not reach here")
        except WSGIHTTPException, resp:
            self.assertEquals(resp.status_int, 422)

    def test_HEAD(self):
        """ Test swift.chunk.server.ChunkController.HEAD """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id
        req = Request.blank(chunk_url + '/whatever')
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 400)

        req = Request.blank('/')
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 400)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 404)

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 200)
        self.assertEquals(resp.content_length, 6)
        self.assertEquals(resp.content_type, 'application/octet-stream')
        self.assertEquals(resp.headers['content-type'],
                           'application/octet-stream')
        self.assertEquals(resp.headers['etag'],
                          '"0b4c12d7e0a73840c1c4f148fda3b037"')

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition, 
                                                    chunk_id),
                                  chunk_server.CHUNK_IMAGE_FILE)
        os.unlink(chunk_file)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 404)

    def test_HEAD_quarantine_zbyte(self):
        """ Test swift.chunk.server.ChunkController.HEAD """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                      FakeLogger(), keep_data_fp=True)

        file_name = os.path.basename(cf.data_file)
        with open(cf.data_file) as fp:
            metadata = chunk_server.read_metadata(fp)
        os.unlink(cf.data_file)
        with open(cf.data_file, 'w') as fp:
            chunk_server.write_metadata(fp, metadata)

        self.assertEquals(os.listdir(cf.datadir)[0], file_name)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.HEAD(req)
        self.assertEquals(resp.status_int, 404)

        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                       os.path.basename(os.path.dirname(cf.data_file)))
        self.assertEquals(os.listdir(quar_dir)[0], file_name)

    def test_GET(self):
        """ Test swift.chunk.server.ChunkController.GET """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        req = Request.blank(chunk_url + '/whatever')
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 400)

        req = Request.blank('/')
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 400)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 404)

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 200)
        self.assertEquals(resp.body, 'VERIFY')
        self.assertEquals(resp.content_length, 6)
        self.assertEquals(resp.content_type, 'application/octet-stream')
        self.assertEquals(resp.headers['content-type'],
                                        'application/octet-stream')
        self.assertEquals(resp.headers['etag'],
                          '"0b4c12d7e0a73840c1c4f148fda3b037"')

        req = Request.blank(chunk_url)
        req.range = 'bytes=1-3'
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 206)
        self.assertEquals(resp.body, 'ERI')
        self.assertEquals(resp.headers['content-length'], '3')

        req = Request.blank(chunk_url)
        req.range = 'bytes=1-'
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 206)
        self.assertEquals(resp.body, 'ERIFY')
        self.assertEquals(resp.headers['content-length'], '5')

        req = Request.blank(chunk_url)
        req.range = 'bytes=-2'
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 206)
        self.assertEquals(resp.body, 'FY')
        self.assertEquals(resp.headers['content-length'], '2')

        datadir = os.path.join(self.testdir, device, chunk_server.DATADIR)
        chunk_file = os.path.join(storage_directory(datadir, partition,
                                                    chunk_id),
                                  chunk_server.CHUNK_IMAGE_FILE)
        os.unlink(chunk_file)
        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 404)

    def test_GET_quarantine(self):
        """ Test swift.chunk.server.ChunkController.GET """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                      FakeLogger(), keep_data_fp=True)

        file_name = os.path.basename(cf.data_file)
        etag = md5()
        etag.update('VERIF')
        etag = etag.hexdigest()
        metadata = {
            'X-Timestamp': timestamp,
            'ETag': etag,
            'Content-Length': 6,
        }
        chunk_server.write_metadata(cf.fp, metadata)
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                       os.path.basename(os.path.dirname(cf.data_file)))
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)
        body = resp.body  # actually does quarantining
        self.assertEquals(body, 'VERIFY')
        self.assertEquals(os.listdir(quar_dir)[0], file_name)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 404)

    def test_GET_quarantine_zbyte(self):
        """ Test swift.chunk.server.ChunkController.GET """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                      FakeLogger(), keep_data_fp=True)

        file_name = os.path.basename(cf.data_file)
        with open(cf.data_file) as fp:
            metadata = chunk_server.read_metadata(fp)
        os.unlink(cf.data_file)
        with open(cf.data_file, 'w') as fp:
            chunk_server.write_metadata(fp, metadata)

        self.assertEquals(os.listdir(cf.datadir)[0], file_name)

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 404)

        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                       os.path.basename(os.path.dirname(cf.data_file)))
        self.assertEquals(os.listdir(quar_dir)[0], file_name)

    def test_GET_quarantine_range(self):
        """ Test swift.chunk.server.ChunkController.GET """
        device = 'sda1'
        partition = '0'
        path = 'acc/container/obj'
        chunk_id = '188a5356a091db7465c484a0e8ccd23b6d015b10a956128851'\
                   '591082116a240b'
        chunk_url = '/' + device + '/' + partition + '/' + \
                    path + '/' + chunk_id

        timestamp = normalize_timestamp(time())
        req = Request.blank(chunk_url,  environ={'REQUEST_METHOD': 'STORE'},
                headers={'X-Timestamp': timestamp,
                         'Content-Type': 'application/octet-stream'})
        req.body = 'VERIFY'
        resp = self.chunk_controller.STORE(req)
        self.assertEquals(resp.status_int, 201)

        cf = chunk_server.ChunkFile(self.testdir, device, partition, chunk_id,
                                      FakeLogger(), keep_data_fp=True)

        file_name = os.path.basename(cf.data_file)
        etag = md5()
        etag.update('VERIF')
        etag = etag.hexdigest()
        metadata = {
            'X-Timestamp': timestamp,
            'X-Chunk-Last-Access': timestamp,
            'ETag': etag,
            'Content-Length': 6,
        }
        chunk_server.write_metadata(cf.fp, metadata)
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)

        req = Request.blank(chunk_url)
        req.range = 'bytes=0-4'  # partial
        resp = self.chunk_controller.GET(req)
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                       os.path.basename(os.path.dirname(cf.data_file)))
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)
        self.assertFalse(os.path.isdir(quar_dir))

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 200)

        req = Request.blank(chunk_url)
        req.range = 'bytes=1-6'  # partial
        resp = self.chunk_controller.GET(req)
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                            os.path.basename(os.path.dirname(cf.data_file)))
        body = resp.body
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)
        self.assertFalse(os.path.isdir(quar_dir))
 
        req = Request.blank(chunk_url)
        req.range = 'bytes=0-14'  # full
        resp = self.chunk_controller.GET(req)
        quar_dir = os.path.join(self.testdir, device, 'quarantined', 'chunks',
                       os.path.basename(os.path.dirname(cf.data_file)))
        self.assertEquals(os.listdir(cf.datadir)[0], file_name)
        body = resp.body
        self.assertTrue(os.path.isdir(quar_dir))

        req = Request.blank(chunk_url)
        resp = self.chunk_controller.GET(req)
        self.assertEquals(resp.status_int, 404)

#    def test_call(self):
#        """ Test swift.chunk.server.ChunkController.__call__ """
#        inbuf = StringIO()
#        errbuf = StringIO()
#        outbuf = StringIO()
#
#        def start_response(*args):
#            """ Sends args to outbuf """
#            outbuf.writelines(args)
#
#        self.chunk_controller.__call__({'REQUEST_METHOD': 'STORE',
#                                         'SCRIPT_NAME': '',
#                                         'PATH_INFO': '/sda1/p/a/c/o',
#                                         'SERVER_NAME': '127.0.0.1',
#                                         'SERVER_PORT': '8080',
#                                         'SERVER_PROTOCOL': 'HTTP/1.0',
#                                         'CONTENT_LENGTH': '0',
#                                         'wsgi.version': (1, 0),
#                                         'wsgi.url_scheme': 'http',
#                                         'wsgi.input': inbuf,
#                                         'wsgi.errors': errbuf,
#                                         'wsgi.multithread': False,
#                                         'wsgi.multiprocess': False,
#                                         'wsgi.run_once': False},
#                                        start_response)
#        self.assertEquals(errbuf.getvalue(), '')
#        self.assertEquals(outbuf.getvalue()[:4], '400 ')
#
#        inbuf = StringIO()
#        errbuf = StringIO()
#        outbuf = StringIO()
#        self.chunk_controller.__call__({'REQUEST_METHOD': 'GET',
#                                         'SCRIPT_NAME': '',
#                                         'PATH_INFO': '/sda1/p/a/c/o',
#                                         'SERVER_NAME': '127.0.0.1',
#                                         'SERVER_PORT': '8080',
#                                         'SERVER_PROTOCOL': 'HTTP/1.0',
#                                         'CONTENT_LENGTH': '0',
#                                         'wsgi.version': (1, 0),
#                                         'wsgi.url_scheme': 'http',
#                                         'wsgi.input': inbuf,
#                                         'wsgi.errors': errbuf,
#                                         'wsgi.multithread': False,
#                                         'wsgi.multiprocess': False,
#                                         'wsgi.run_once': False},
#                                        start_response)
#        self.assertEquals(errbuf.getvalue(), '')
#        self.assertEquals(outbuf.getvalue()[:4], '404 ')
#
#        inbuf = StringIO()
#        errbuf = StringIO()
#        outbuf = StringIO()
#        self.chunk_controller.__call__({'REQUEST_METHOD': 'INVALID',
#                                         'SCRIPT_NAME': '',
#                                         'PATH_INFO': '/sda1/p/a/c/o',
#                                         'SERVER_NAME': '127.0.0.1',
#                                         'SERVER_PORT': '8080',
#                                         'SERVER_PROTOCOL': 'HTTP/1.0',
#                                         'CONTENT_LENGTH': '0',
#                                         'wsgi.version': (1, 0),
#                                         'wsgi.url_scheme': 'http',
#                                         'wsgi.input': inbuf,
#                                         'wsgi.errors': errbuf,
#                                         'wsgi.multithread': False,
#                                         'wsgi.multiprocess': False,
#                                         'wsgi.run_once': False},
#                                        start_response)
#        self.assertEquals(errbuf.getvalue(), '')
#        self.assertEquals(outbuf.getvalue()[:4], '405 ')
# 
#    def test_chunked_put(self):
#        listener = listen(('localhost', 0))
#        port = listener.getsockname()[1]
#        killer = spawn(wsgi.server, listener, self.chunk_controller,
#                       NullLogger())
#        sock = connect_tcp(('localhost', port))
#        fd = sock.makefile()
#        fd.write('PUT /sda1/p/a/c/o HTTP/1.1\r\nHost: localhost\r\n'
#                 'Content-Type: text/plain\r\n'
#                 'Connection: close\r\nX-Timestamp: 1.0\r\n'
#                 'Transfer-Encoding: chunked\r\n\r\n'
#                 '2\r\noh\r\n4\r\n hai\r\n0\r\n\r\n')
#        fd.flush()
#        readuntil2crlfs(fd)
#        sock = connect_tcp(('localhost', port))
#        fd = sock.makefile()
#        fd.write('GET /sda1/p/a/c/o HTTP/1.1\r\nHost: localhost\r\n'
#                 'Connection: close\r\n\r\n')
#        fd.flush()
#        readuntil2crlfs(fd)
#        response = fd.read()
#        self.assertEquals(response, 'oh hai')
#        killer.kill()
# 
#    def test_max_object_name_length(self):
#        timestamp = normalize_timestamp(time())
#        req = Request.blank('/sda1/p/a/c/' + ('1' * 1024),
#                environ={'REQUEST_METHOD': 'STORE'},
#                headers={'X-Timestamp': timestamp,
#                         'Content-Length': '4',
#                         'Content-Type': 'application/octet-stream'})
#        req.body = 'DATA'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        req = Request.blank('/sda1/p/a/c/' + ('2' * 1025),
#                environ={'REQUEST_METHOD': 'STORE'},
#                headers={'X-Timestamp': timestamp,
#                         'Content-Length': '4',
#                         'Content-Type': 'application/octet-stream'})
#        req.body = 'DATA'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 400)
# 
#    def test_max_upload_time(self):
# 
#        class SlowBody():
# 
#            def __init__(self):
#                self.sent = 0
# 
#            def read(self, size=-1):
#                if self.sent < 4:
#                    sleep(0.1)
#                    self.sent += 1
#                    return ' '
#                return ''
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'STORE', 'wsgi.input': SlowBody()},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4', 'Content-Type': 'text/plain'})
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.chunk_controller.max_upload_time = 0.1
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'STORE', 'wsgi.input': SlowBody()},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4', 'Content-Type': 'text/plain'})
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 408)
# 
#    def test_short_body(self):
# 
#        class ShortBody():
# 
#            def __init__(self):
#                self.sent = False
# 
#            def read(self, size=-1):
#                if not self.sent:
#                    self.sent = True
#                    return '   '
#                return ''
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'STORE', 'wsgi.input': ShortBody()},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4', 'Content-Type': 'text/plain'})
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 499)
# 
#    def test_bad_sinces(self):
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4', 'Content-Type': 'text/plain'},
#            body='    ')
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'},
#            headers={'If-Unmodified-Since': 'Not a valid date'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 200)
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'},
#            headers={'If-Modified-Since': 'Not a valid date'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 200)
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'},
#            headers={'If-Unmodified-Since': 'Sat, 29 Oct 1000 19:43:31 GMT'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 412)
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'},
#            headers={'If-Modified-Since': 'Sat, 29 Oct 1000 19:43:31 GMT'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 412)
# 
#    def test_content_encoding(self):
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4', 'Content-Type': 'text/plain',
#                     'Content-Encoding': 'gzip'},
#            body='    ')
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 200)
#        self.assertEquals(resp.headers['content-encoding'], 'gzip')
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD':
#            'HEAD'})
#        resp = self.chunk_controller.HEAD(req)
#        self.assertEquals(resp.status_int, 200)
#        self.assertEquals(resp.headers['content-encoding'], 'gzip')
# 
#    def test_manifest_header(self):
#        timestamp = normalize_timestamp(time())
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#                headers={'X-Timestamp': timestamp,
#                         'Content-Type': 'text/plain',
#                         'Content-Length': '0',
#                         'X-Object-Manifest': 'c/o/'})
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        objfile = os.path.join(self.testdir, 'sda1',
#            storage_directory(chunk_server.DATADIR, 'p', hash_path('a', 'c',
#            'o')), timestamp + '.data')
#        self.assert_(os.path.isfile(objfile))
#        self.assertEquals(pickle.loads(getxattr(objfile,
#            chunk_server.METADATA_KEY)), {'X-Timestamp': timestamp,
#            'Content-Length': '0', 'Content-Type': 'text/plain', 'name':
#            '/a/c/o', 'X-Object-Manifest': 'c/o/', 'ETag':
#            'd41d8cd98f00b204e9800998ecf8427e'})
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 200)
#        self.assertEquals(resp.headers.get('x-object-manifest'), 'c/o/')
# 
#    def test_async_update_http_connect(self):
#        given_args = []
# 
#        def fake_http_connect(*args):
#            given_args.extend(args)
#            raise Exception('test')
# 
#        orig_http_connect = chunk_server.http_connect
#        try:
#            chunk_server.http_connect = fake_http_connect
#            self.chunk_controller.async_update('PUT', 'a', 'c', 'o',
#                '127.0.0.1:1234', 1, 'sdc1',
#                {'x-timestamp': '1', 'x-out': 'set'}, 'sda1')
#        finally:
#            chunk_server.http_connect = orig_http_connect
#        self.assertEquals(given_args, ['127.0.0.1', '1234', 'sdc1', 1, 'PUT',
#            '/a/c/o', {'x-timestamp': '1', 'x-out': 'set'}])
# 
#    def test_async_update_saves_on_exception(self):
# 
#        def fake_http_connect(*args):
#            raise Exception('test')
# 
#        orig_http_connect = chunk_server.http_connect
#        try:
#            chunk_server.http_connect = fake_http_connect
#            self.chunk_controller.async_update('PUT', 'a', 'c', 'o',
#                '127.0.0.1:1234', 1, 'sdc1',
#                {'x-timestamp': '1', 'x-out': 'set'}, 'sda1')
#        finally:
#            chunk_server.http_connect = orig_http_connect
#        self.assertEquals(
#            pickle.load(open(os.path.join(self.testdir, 'sda1',
#                'async_pending', 'a83',
#                '06fbf0b514e5199dfc4e00f42eb5ea83-0000000001.00000'))),
#            {'headers': {'x-timestamp': '1', 'x-out': 'set'}, 'account': 'a',
#             'container': 'c', 'obj': 'o', 'op': 'PUT'})
# 
#    def test_async_update_saves_on_non_2xx(self):
# 
#        def fake_http_connect(status):
# 
#            class FakeConn(object):
# 
#                def __init__(self, status):
#                    self.status = status
# 
#                def getresponse(self):
#                    return self
# 
#                def read(self):
#                    return ''
# 
#            return lambda *args: FakeConn(status)
# 
#        orig_http_connect = chunk_server.http_connect
#        try:
#            for status in (199, 300, 503):
#                chunk_server.http_connect = fake_http_connect(status)
#                self.chunk_controller.async_update('PUT', 'a', 'c', 'o',
#                    '127.0.0.1:1234', 1, 'sdc1',
#                    {'x-timestamp': '1', 'x-out': str(status)}, 'sda1')
#                self.assertEquals(
#                    pickle.load(open(os.path.join(self.testdir, 'sda1',
#                        'async_pending', 'a83',
#                        '06fbf0b514e5199dfc4e00f42eb5ea83-0000000001.00000'))),
#                    {'headers': {'x-timestamp': '1', 'x-out': str(status)},
#                     'account': 'a', 'container': 'c', 'obj': 'o',
#                     'op': 'PUT'})
#        finally:
#            chunk_server.http_connect = orig_http_connect
# 
#    def test_async_update_does_not_save_on_2xx(self):
# 
#        def fake_http_connect(status):
# 
#            class FakeConn(object):
# 
#                def __init__(self, status):
#                    self.status = status
# 
#                def getresponse(self):
#                    return self
# 
#                def read(self):
#                    return ''
# 
#            return lambda *args: FakeConn(status)
# 
#        orig_http_connect = chunk_server.http_connect
#        try:
#            for status in (200, 299):
#                chunk_server.http_connect = fake_http_connect(status)
#                self.chunk_controller.async_update('PUT', 'a', 'c', 'o',
#                    '127.0.0.1:1234', 1, 'sdc1',
#                    {'x-timestamp': '1', 'x-out': str(status)}, 'sda1')
#                self.assertFalse(
#                    os.path.exists(os.path.join(self.testdir, 'sda1',
#                        'async_pending', 'a83',
#                        '06fbf0b514e5199dfc4e00f42eb5ea83-0000000001.00000')))
#        finally:
#            chunk_server.http_connect = orig_http_connect
# 
#    def test_delete_at_update_put(self):
#        given_args = []
# 
#        def fake_async_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.async_update = fake_async_update
#        self.chunk_controller.delete_at_update('PUT', 2, 'a', 'c', 'o',
#            {'x-timestamp': '1'}, 'sda1')
#        self.assertEquals(given_args, ['PUT', '.expiring_objects', '0',
#            '2-a/c/o', None, None, None,
#            {'x-size': '0', 'x-etag': 'd41d8cd98f00b204e9800998ecf8427e',
#             'x-content-type': 'text/plain', 'x-timestamp': '1',
#             'x-trans-id': '-'},
#            'sda1'])
# 
#    def test_delete_at_update_put_with_info(self):
#        given_args = []
# 
#        def fake_async_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.async_update = fake_async_update
#        self.chunk_controller.delete_at_update('PUT', 2, 'a', 'c', 'o',
#            {'x-timestamp': '1', 'X-Delete-At-Host': '127.0.0.1:1234',
#             'X-Delete-At-Partition': '3', 'X-Delete-At-Device': 'sdc1'},
#            'sda1')
#        self.assertEquals(given_args, ['PUT', '.expiring_objects', '0',
#            '2-a/c/o', '127.0.0.1:1234', '3', 'sdc1',
#            {'x-size': '0', 'x-etag': 'd41d8cd98f00b204e9800998ecf8427e',
#             'x-content-type': 'text/plain', 'x-timestamp': '1',
#             'x-trans-id': '-'},
#            'sda1'])
# 
#    def test_delete_at_update_delete(self):
#        given_args = []
# 
#        def fake_async_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.async_update = fake_async_update
#        self.chunk_controller.delete_at_update('DELETE', 2, 'a', 'c', 'o',
#            {'x-timestamp': '1'}, 'sda1')
#        self.assertEquals(given_args, ['DELETE', '.expiring_objects', '0',
#            '2-a/c/o', None, None, None,
#            {'x-timestamp': '1', 'x-trans-id': '-'}, 'sda1'])
# 
#    def test_POST_calls_delete_at(self):
#        given_args = []
# 
#        def fake_delete_at_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.delete_at_update = fake_delete_at_update
# 
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.assertEquals(given_args, [])
# 
#        sleep(.00001)
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'POST'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Type': 'application/x-test'})
#        resp = self.chunk_controller.POST(req)
#        self.assertEquals(resp.status_int, 202)
#        self.assertEquals(given_args, [])
# 
#        sleep(.00001)
#        timestamp1 = normalize_timestamp(time())
#        delete_at_timestamp1 = str(int(time() + 1000))
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'POST'},
#            headers={'X-Timestamp': timestamp1,
#                     'Content-Type': 'application/x-test',
#                     'X-Delete-At': delete_at_timestamp1})
#        resp = self.chunk_controller.POST(req)
#        self.assertEquals(resp.status_int, 202)
#        self.assertEquals(given_args, [
#            'PUT', int(delete_at_timestamp1), 'a', 'c', 'o',
#            {'X-Delete-At': delete_at_timestamp1,
#             'Content-Type': 'application/x-test',
#             'X-Timestamp': timestamp1,
#             'Host': 'localhost:80'},
#            'sda1'])
# 
#        while given_args:
#            given_args.pop()
# 
#        sleep(.00001)
#        timestamp2 = normalize_timestamp(time())
#        delete_at_timestamp2 = str(int(time() + 2000))
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'POST'},
#            headers={'X-Timestamp': timestamp2,
#                     'Content-Type': 'application/x-test',
#                     'X-Delete-At': delete_at_timestamp2})
#        resp = self.chunk_controller.POST(req)
#        self.assertEquals(resp.status_int, 202)
#        self.assertEquals(given_args, [
#            'PUT', int(delete_at_timestamp2), 'a', 'c', 'o',
#            {'X-Delete-At': delete_at_timestamp2,
#             'Content-Type': 'application/x-test',
#             'X-Timestamp': timestamp2, 'Host': 'localhost:80'},
#            'sda1',
#            'DELETE', int(delete_at_timestamp1), 'a', 'c', 'o',
#            # This 2 timestamp is okay because it's ignored since it's just
#            # part of the current request headers. The above 1 timestamp is the
#            # important one.
#            {'X-Delete-At': delete_at_timestamp2,
#             'Content-Type': 'application/x-test',
#             'X-Timestamp': timestamp2, 'Host': 'localhost:80'},
#            'sda1'])
# 
#    def test_STORE_calls_delete_at(self):
#        given_args = []
# 
#        def fake_delete_at_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.delete_at_update = fake_delete_at_update
# 
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.assertEquals(given_args, [])
# 
#        sleep(.00001)
#        timestamp1 = normalize_timestamp(time())
#        delete_at_timestamp1 = str(int(time() + 1000))
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': timestamp1,
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream',
#                     'X-Delete-At': delete_at_timestamp1})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.assertEquals(given_args, [
#            'PUT', int(delete_at_timestamp1), 'a', 'c', 'o',
#            {'X-Delete-At': delete_at_timestamp1,
#             'Content-Length': '4',
#             'Content-Type': 'application/octet-stream',
#             'X-Timestamp': timestamp1,
#             'Host': 'localhost:80'},
#            'sda1'])
# 
#        while given_args:
#            given_args.pop()
# 
#        sleep(.00001)
#        timestamp2 = normalize_timestamp(time())
#        delete_at_timestamp2 = str(int(time() + 2000))
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': timestamp2,
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream',
#                     'X-Delete-At': delete_at_timestamp2})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.assertEquals(given_args, [
#            'PUT', int(delete_at_timestamp2), 'a', 'c', 'o',
#            {'X-Delete-At': delete_at_timestamp2,
#             'Content-Length': '4',
#             'Content-Type': 'application/octet-stream',
#             'X-Timestamp': timestamp2, 'Host': 'localhost:80'},
#            'sda1',
#            'DELETE', int(delete_at_timestamp1), 'a', 'c', 'o',
#            # This 2 timestamp is okay because it's ignored since it's just
#            # part of the current request headers. The above 1 timestamp is the
#            # important one.
#            {'X-Delete-At': delete_at_timestamp2,
#             'Content-Length': '4',
#             'Content-Type': 'application/octet-stream',
#             'X-Timestamp': timestamp2, 'Host': 'localhost:80'},
#            'sda1'])
# 
#    def test_GET_but_expired(self):
#        test_time = time() + 10000
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 2000),
#                     'X-Delete-At': str(int(test_time + 100)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'GET'},
#            headers={'X-Timestamp': normalize_timestamp(test_time)})
#        resp = self.chunk_controller.GET(req)
#        self.assertEquals(resp.status_int, 200)
# 
#        orig_time = chunk_server.time.time
#        try:
#            t = time()
#            chunk_server.time.time = lambda: t
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'STORE'},
#                headers={'X-Timestamp': normalize_timestamp(test_time - 1000),
#                         'X-Delete-At': str(int(t + 1)),
#                         'Content-Length': '4',
#                         'Content-Type': 'application/octet-stream'})
#            req.body = 'TEST'
#            resp = self.chunk_controller.STORE(req)
#            self.assertEquals(resp.status_int, 201)
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'GET'},
#                headers={'X-Timestamp': normalize_timestamp(test_time)})
#            resp = self.chunk_controller.GET(req)
#            self.assertEquals(resp.status_int, 200)
#        finally:
#            chunk_server.time.time = orig_time
# 
#        orig_time = chunk_server.time.time
#        try:
#            t = time() + 2
#            chunk_server.time.time = lambda: t
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'GET'},
#                headers={'X-Timestamp': normalize_timestamp(t)})
#            resp = self.chunk_controller.GET(req)
#            self.assertEquals(resp.status_int, 404)
#        finally:
#            chunk_server.time.time = orig_time
# 
#    def test_HEAD_but_expired(self):
#        test_time = time() + 10000
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 2000),
#                     'X-Delete-At': str(int(test_time + 100)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'HEAD'},
#            headers={'X-Timestamp': normalize_timestamp(test_time)})
#        resp = self.chunk_controller.HEAD(req)
#        self.assertEquals(resp.status_int, 200)
# 
#        orig_time = chunk_server.time.time
#        try:
#            t = time()
#            chunk_server.time.time = lambda: t
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'STORE'},
#                headers={'X-Timestamp': normalize_timestamp(test_time - 1000),
#                         'X-Delete-At': str(int(t + 1)),
#                         'Content-Length': '4',
#                         'Content-Type': 'application/octet-stream'})
#            req.body = 'TEST'
#            resp = self.chunk_controller.STORE(req)
#            self.assertEquals(resp.status_int, 201)
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'HEAD'},
#                headers={'X-Timestamp': normalize_timestamp(test_time)})
#            resp = self.chunk_controller.HEAD(req)
#            self.assertEquals(resp.status_int, 200)
#        finally:
#            chunk_server.time.time = orig_time
# 
#        orig_time = chunk_server.time.time
#        try:
#            t = time() + 2
#            chunk_server.time.time = lambda: t
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'HEAD'},
#                headers={'X-Timestamp': normalize_timestamp(time())})
#            resp = self.chunk_controller.HEAD(req)
#            self.assertEquals(resp.status_int, 404)
#        finally:
#            chunk_server.time.time = orig_time
# 
#    def test_POST_but_expired(self):
#        test_time = time() + 10000
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 2000),
#                     'X-Delete-At': str(int(test_time + 100)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'POST'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 1500)})
#        resp = self.chunk_controller.POST(req)
#        self.assertEquals(resp.status_int, 202)
# 
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 1000),
#                     'X-Delete-At': str(int(time() + 1)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        orig_time = chunk_server.time.time
#        try:
#            t = time() + 2
#            chunk_server.time.time = lambda: t
#            req = Request.blank('/sda1/p/a/c/o',
#                environ={'REQUEST_METHOD': 'POST'},
#                headers={'X-Timestamp': normalize_timestamp(time())})
#            resp = self.chunk_controller.POST(req)
#            self.assertEquals(resp.status_int, 404)
#        finally:
#            chunk_server.time.time = orig_time
# 
#    def test_DELETE_if_delete_at(self):
#        test_time = time() + 10000
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 99),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 98)})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 204)
# 
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 97),
#                     'X-Delete-At': str(int(test_time - 1)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 95),
#                     'X-If-Delete-At': str(int(test_time))})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 412)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 95)})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 204)
# 
#        delete_at_timestamp = str(int(test_time - 1))
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 94),
#                     'X-Delete-At': delete_at_timestamp,
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 92),
#                     'X-If-Delete-At': str(int(test_time))})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 412)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': normalize_timestamp(test_time - 92),
#                     'X-If-Delete-At': delete_at_timestamp})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 204)
# 
#    def test_DELETE_calls_delete_at(self):
#        given_args = []
# 
#        def fake_delete_at_update(*args):
#            given_args.extend(args)
# 
#        self.chunk_controller.delete_at_update = fake_delete_at_update
# 
#        timestamp1 = normalize_timestamp(time())
#        delete_at_timestamp1 = str(int(time() + 1000))
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': timestamp1,
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream',
#                     'X-Delete-At': delete_at_timestamp1})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
#        self.assertEquals(given_args, [
#            'PUT', int(delete_at_timestamp1), 'a', 'c', 'o',
#            {'X-Delete-At': delete_at_timestamp1,
#             'Content-Length': '4',
#             'Content-Type': 'application/octet-stream',
#             'X-Timestamp': timestamp1,
#             'Host': 'localhost:80'},
#            'sda1'])
# 
#        while given_args:
#            given_args.pop()
# 
#        sleep(.00001)
#        timestamp2 = normalize_timestamp(time())
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'DELETE'},
#            headers={'X-Timestamp': timestamp2,
#                     'Content-Type': 'application/octet-stream'})
#        resp = self.chunk_controller.DELETE(req)
#        self.assertEquals(resp.status_int, 204)
#        self.assertEquals(given_args, [
#            'DELETE', int(delete_at_timestamp1), 'a', 'c', 'o',
#            {'Content-Type': 'application/octet-stream',
#             'Host': 'localhost:80', 'X-Timestamp': timestamp2},
#            'sda1'])
# 
#    def test_STORE_delete_at_in_past(self):
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'X-Delete-At': str(int(time() - 1)),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 400)
#        self.assertTrue('X-Delete-At in past' in resp.body)
# 
#    def test_POST_delete_at_in_past(self):
#        req = Request.blank('/sda1/p/a/c/o', environ={'REQUEST_METHOD': 'STORE'},
#            headers={'X-Timestamp': normalize_timestamp(time()),
#                     'Content-Length': '4',
#                     'Content-Type': 'application/octet-stream'})
#        req.body = 'TEST'
#        resp = self.chunk_controller.STORE(req)
#        self.assertEquals(resp.status_int, 201)
# 
#        req = Request.blank('/sda1/p/a/c/o',
#            environ={'REQUEST_METHOD': 'POST'},
#            headers={'X-Timestamp': normalize_timestamp(time() + 1),
#                     'X-Delete-At': str(int(time() - 1))})
#        resp = self.chunk_controller.POST(req)
#        self.assertEquals(resp.status_int, 400)
#        self.assertTrue('X-Delete-At in past' in resp.body)
# 
#    def test_REPLICATE_works(self):
# 
#        def fake_get_hashes(*args, **kwargs):
#            return 0, {1: 2}
# 
#        def my_tpool_execute(*args, **kwargs):
#            func = args[0]
#            args = args[1:]
#            return func(*args, **kwargs)
# 
#        was_get_hashes = replicator.get_hashes
#        replicator.get_hashes = fake_get_hashes
#        was_tpool_exe = tpool.execute
#        tpool.execute = my_tpool_execute
#        try:
#            req = Request.blank('/sda1/p/suff',
#                environ={'REQUEST_METHOD': 'REPLICATE'},
#                headers={})
#            resp = self.chunk_controller.REPLICATE(req)
#            self.assertEquals(resp.status_int, 200)
#            p_data = pickle.loads(resp.body)
#            self.assertEquals(p_data, {1: 2})
#        finally:
#            tpool.execute = was_tpool_exe
#            replicator.get_hashes = was_get_hashes
# 
#    def test_REPLICATE_timeout(self):
# 
#        def fake_get_hashes(*args, **kwargs):
#            raise Timeout()
# 
#        def my_tpool_execute(*args, **kwargs):
#            func = args[0]
#            args = args[1:]
#            return func(*args, **kwargs)
# 
#        was_get_hashes = replicator.get_hashes
#        replicator.get_hashes = fake_get_hashes
#        was_tpool_exe = tpool.execute
#        tpool.execute = my_tpool_execute
#        try:
#            req = Request.blank('/sda1/p/suff',
#                environ={'REQUEST_METHOD': 'REPLICATE'},
#                headers={})
#            self.assertRaises(Timeout, self.chunk_controller.REPLICATE, req)
#        finally:
#            tpool.execute = was_tpool_exe
#            replicator.get_hashes = was_get_hashes
#===============================================================================

if __name__ == '__main__':
    unittest.main()
